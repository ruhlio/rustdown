#![feature(plugin)]
#![feature(collections)]
#![feature(core)]

#[plugin]
#[no_link]
extern crate regex_macros;
extern crate regex;

use std::iter::Peekable;

static UNORDERED_LIST_REGEX: regex::Regex = regex!(r"^[*+-]+\s+");
static ORDERED_LIST_REGEX: regex::Regex = regex!(r"^\d+\.\s+");
static SETEXT_HEADER_REGEX: regex::Regex = regex!(r"^\s*(=|-)(=|-| )*$");
static BLOCK_QUOTE_REGEX: regex::Regex = regex!(r"^>\s");

#[derive(Debug, PartialEq)]
enum Block<'a> {
   Code(&'a str),
   HorizontalRule,
   Heading { level: usize, text: &'a str },
   BlockQuote(Vec<Block<'a>>),
   UnorderedList(&'a str),
   OrderedList(&'a str),
//   Html(&'a str),
   Text(&'a str),
}

struct ParseStep<'a> {
   current_indent: usize,
   line: &'a str,
   next_line: Option<&'a str>,
   index: usize,
}

fn parse<'a>(raw_markdown: &'a str) {
   let lines: Vec<&'a str> = raw_markdown.lines().collect();
   let blocks = parse_lines(lines);
}

fn parse_lines<'a>(lines: Vec<&'a str>) -> Vec<Block<'a>>
{
   let mut blocks = Vec::with_capacity(lines.len());
   let mut previous_indent = 0;
   let mut index = 0;
   while index < lines.len() {
      let (current_indent, line) = parse_indentation(lines[index]);
      let next_line = if index + 1 < lines.len() {
         let (_, next_line) = parse_indentation(lines[index + 1]);
         Some(next_line)
      }
      else { None };
      let parse_step = ParseStep {
         current_indent: current_indent,
         line: line,
         next_line: next_line,
         index: index,
      };

      let (index_increment, block) = parse_block(parse_step, &lines);
      index += index_increment;
      blocks.push(block);
      previous_indent = current_indent;
   }

   return blocks;
}

fn parse_block<'a>(parse_step: ParseStep<'a>, lines: &Vec<&'a str>) -> (usize, Block<'a>) {
   let ParseStep{ line, index, .. } = parse_step;

   if line.char_at(0) == '#' {
      (1, parse_atx_heading(line))
   }
   else if BLOCK_QUOTE_REGEX.is_match(line) {
      parse_blockquote(parse_step, lines)
   }
   else if UNORDERED_LIST_REGEX.is_match(line) {
      (1, parse_unordered_list(line))
   }
   else if ORDERED_LIST_REGEX.is_match(line) {
      (1, parse_ordered_list(line))
   }
   else {
      match parse_step.next_line {
         Some(next_line) if SETEXT_HEADER_REGEX.is_match(next_line) =>
            (2, parse_setext_heading(line, next_line)),
         _ => (1, parse_text(line))
      }
   }
}

fn parse_atx_heading<'a>(line: &'a str) -> Block<'a> {
   let level = line.chars().take_while(|&ch| ch == '#' ).count();
   let stripped = line.slice_from(level).trim();
   Block::Heading { level: level, text: stripped }
}

// Allows mixing of '=' & '-', the first character determines level
fn parse_setext_heading<'a>(line: &'a str, next_line: &'a str) -> Block<'a> {
   let level = if next_line.trim_left().char_at(0) == '=' { 1 } else { 2 };
   Block::Heading { level: level, text: line.trim() }
}

fn parse_blockquote<'a>(parse_step: ParseStep<'a>, lines: &Vec<&'a str>) -> (usize, Block<'a>) {
   let inner_lines = lines
      .slice_from(parse_step.index)
      .iter()
      .take_while(|&line| BLOCK_QUOTE_REGEX.is_match(line))
      .map(|&line| line.slice_from(2))
      .collect();
   let inner_blocks = parse_lines(inner_lines);
   (inner_blocks.len(), Block::BlockQuote(inner_blocks))
}

fn parse_unordered_list<'a>(line: &'a str) -> Block<'a> {
   Block::UnorderedList(line)
}

fn parse_ordered_list<'a>(line: &'a str) -> Block<'a> {
   Block::OrderedList(line)
}

fn parse_text<'a>(line: &'a str) -> Block<'a> {
   Block::Text(line)
}

fn parse_indentation(line: &str) -> (usize, &str) {
   let mut pos = 0;
   for (index, ch) in line.chars().enumerate() {
      if ch == ' ' || ch == '\t' {
         pos += 1;
      }
      else {
         break;
      }
   }

   (pos, line.slice_from(pos))
}

#[test]
fn indentation_parses() {
   assert_eq!((0, ""), parse_indentation(""));
   assert_eq!((2, ""), parse_indentation("  "));
   assert_eq!((3, "hi"), parse_indentation("   hi"));
   assert_eq!((1, ""), parse_indentation("\t"));
   assert_eq!((4, "boo "), parse_indentation("\t\t\t\tboo "));
//   assert_eq!((3, "heyo  "), parse_indentation(2, " > heyo  "));
}

fn test_parse_blocks(markdown: &str) -> Vec<Block> {
   let lines: Vec<&str> = markdown.lines().collect();
   parse_lines(lines)
}

#[test]
fn atx_heading_parses() {
   let markdown = "### Hello\nwhat what";
   let blocks = test_parse_blocks(markdown);
   assert_eq!(blocks, vec![
      Block::Heading { level: 3, text: "Hello" },
      Block::Text("what what"),
   ]);
}

#[test]
fn setext_level1_heading_parses() {
   let markdown = "text\n  Heading \n  === \ntext";
   let blocks = test_parse_blocks(markdown);
   assert_eq!(blocks, vec![
      Block::Text("text"),
      Block::Heading { level: 1, text: "Heading" },
      Block::Text("text"),
   ]);
}

#[test]
fn setext_level2_heading_parses() {
   let markdown = "text\n  Heading \n  -=-== \ntext";
   let blocks = test_parse_blocks(markdown);
   assert_eq!(blocks, vec![
      Block::Text("text"),
      Block::Heading { level: 2, text: "Heading" },
      Block::Text("text"),
   ]);
}

#[test]
fn blockquote_parses() {
   let markdown = "text\n> Here lies Diggby Barnibus\n> He was an idiot\n>and a glutton\n herpes";
   let blocks = test_parse_blocks(markdown);
   assert_eq!(blocks, vec![
      Block::Text("text"),
      Block::BlockQuote(vec![
         Block::Text("Here lies Diggby Barnibus"),
         Block::Text("He was an idiot"),
      ]),
      Block::Text(">and a glutton"),
      Block::Text("herpes")
   ]);
}

// #[test]
// fn parse_code() {
//    let markdown = "text\n    let x = 1; \n    let h = 2;\ntext";
//    let blocks = parse_blocks(markdown);
//    assert_eq!(blocks, vec![
//       Block::Text("text".to_string()),
//       Block::Code("    let x = 1; \n    let h = 2;\n".to_string()),
//       Block::Text("text".to_string()),
//    ]);
// }

// #[test]
// fn parse_horizontal_rule() {
//    let markdown = "text\n  *   * ** \ntext";
//    let blocks = parse_blocks(markdown);
//    assert_eq!(blocks, vec![
//       Block::Text("text".to_string()),
//       Block::HorizontalRule,
//       Block::Text("text".to_string()),
//    ]);
// }
